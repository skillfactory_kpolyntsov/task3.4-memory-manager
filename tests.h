#pragma once

#include <cassert>
#include "MemoryManager.h"

void test1()
{
    MemoryManager mm(10); // ##########

    assert(mm.malloc(4) == 0);  // .xxx######
    assert(mm.malloc(4) == 4);  // .xxx.xxx##
    assert(mm.malloc(4) == -1); // .xxx.xxx##

    assert(mm.malloc(3) == -1); // .xxx.xxx##
    assert(mm.malloc(2) == 8);  // .xxx.xxx.x
    assert(mm.malloc(2) == -1); // .xxx.xxx.x

    assert(mm.free(3) == -1); // .xxx.xxx.x
    assert(mm.free(5) == -1); // .xxx.xxx.x
    assert(mm.free(4) == 0);  // .xxx####.x
    assert(mm.free(8) == 0);  // .xxx######

    assert(mm.malloc(5) == 4); // .xxx.xxxx#
    assert(mm.malloc(1) == 9); // .xxx.xxxx.

    assert(mm.free(0) == 0); // ####.xxxx.
    assert(mm.free(4) == 0); // #########.

    assert(mm.malloc(5) == 0); // .xxxx####.
}

void test2()
{
    MemoryManager mm(10);
    for (int i = 0; i < 0; i++)
    {
        assert(mm.free(i) == -1);
    }

    for (int i = 1; i <= 10; i++)
    {
        assert(mm.malloc(1) == i - 1);
    }
    for (int i = 1; i <= 10; i++)
    {
        assert(mm.malloc(1) == -1);
    }
    for (int i = 0; i < 10; i++)
    {
        assert(mm.free(i) == 0);
    }

    assert(mm.malloc(10) == 0);
}

void test3()
{
    MemoryManager mm(10);
    for (int i = 0; i < 0; i++)
    {
        assert(mm.free(i) == -1);
    }

    for (int i = 1; i <= 10; i++)
    {
        assert(mm.malloc(1) == i - 1);
    }
    for (int i = 9; i >= 0; i--)
    {
        assert(mm.free(i) == 0);
    }

    assert(mm.malloc(10) == 0);
}

void test4()
{
    MemoryManager mm(10);
    assert(mm.malloc(11) == -1);
    assert(mm.malloc(-1) == -1);
}
