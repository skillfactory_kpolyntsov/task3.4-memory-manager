#pragma once

#include <iostream>
#include <deque>
#include <stack>
#include <list>

using namespace std;

struct MemoryRange
{
    int index;
    int size;
};

class MemoryManager
{
public:
    MemoryManager(int n);
    ~MemoryManager();
    /**
     * malloc(n), где n — запрашиваемое количество ячеек памяти. 
     * Менеджер должен вернуть номер ячейки, начиная с которой он выделяет отрезок длиной n. 
     * После этого последующие запросы к malloc() не могут выделять ячейки из этого отрезка, 
     * пока он не будет освобождён при помощи free().
     * Если длина наиболее недавнего свободного отрезка меньше n, то malloc() должен вернуть -1.
     */
    int malloc(int n);

    /**
     * free(i), где i — это индекс ячейки, которую когда-то вернул malloc(). 
     * Функция должна вернуть -1, если в ячейке i не начинается никакой выделенный отрезок, и 0 в противном случае.
     */
    int free(int i);

private:
    list<MemoryRange *> availableSpace;
    list<MemoryRange *> reservedSpace;

    void mergeToAvailableSpace(MemoryRange *mRange);

    void print()
    {
        for (auto const &reserved : reservedSpace)
        {
            cout << reserved->index << "::" << reserved->size << "  ";
        }
        cout << endl;
    }
};

MemoryManager::MemoryManager(int n)
{
    availableSpace.push_back(new MemoryRange({0, n}));
}

MemoryManager::~MemoryManager()
{
}

int MemoryManager::malloc(int n)
{
    MemoryRange *available = availableSpace.size() > 0 ? availableSpace.back() : nullptr;
    if (!available || (available->size < n) || n <= 0)
    {
        return -1;
    }
    availableSpace.pop_back();

    MemoryRange *newReserved = new MemoryRange({available->index, n});
    reservedSpace.push_back(newReserved);
    if (available->size > n)
    {
        MemoryRange *newAvailable = new MemoryRange({available->index + n, available->size - n});
        availableSpace.push_back(newAvailable);
    }
    delete available;
    return newReserved->index;
}

int MemoryManager::free(int i)
{
    print();

    list<MemoryRange *>::iterator iReserved = reservedSpace.begin();
    while (iReserved != reservedSpace.end())
    {
        MemoryRange *reserved = *iReserved;
        if (reserved->index == i)
        {
            reservedSpace.erase(iReserved);
            mergeToAvailableSpace(reserved);
            delete reserved;
            return 0;
        }
        iReserved++;
    }

    return -1;
}

void MemoryManager::mergeToAvailableSpace(MemoryRange *mRange)
{
    MemoryRange *newAvailable = new MemoryRange({mRange->index, mRange->size});

    list<MemoryRange *>::iterator iAvailable = availableSpace.begin();
    while (iAvailable != availableSpace.end())
    {
        MemoryRange *available = *iAvailable;

        if (available->index + available->size == newAvailable->index)
        {
            newAvailable->index = available->index;
            newAvailable->size += available->size;
            iAvailable = availableSpace.erase(iAvailable);
            delete available;
        }
        else if (available->index == newAvailable->index + newAvailable->size)
        {
            newAvailable->size += available->size;
            iAvailable = availableSpace.erase(iAvailable);
            delete available;
        }
        else
        {
            iAvailable++;
        }
    }

    availableSpace.push_back(newAvailable);
}